# GUI
Graphical user interface for the LPCA clustering algorithm.

How to use it:
1) clone the folder
2) open the folder from your terminal
3) run "python GUI_START.py"
4) follow the GUI instructions

# Required Python libraries:

- numpy
- tkinter
- matplotlib

# Python version:

Python >= 3.6.4

