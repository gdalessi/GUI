import tkinter as tk 
import numpy as np 
from tkinter import filedialog as fd 

import sys
sys.path.insert(1, 'src')

from utilities import *
import clustering

def GetChoice_scal():
    print("The scaling criterion has been selected.")
    global scaling_criterion
    scaling_criterion = scal.get()
    print(scaling_criterion)

def GetChoice_cent():
    print("The centering criterion has been selected.")
    global centering_criterion
    centering_criterion = cent.get()

def GetChoice_init():
    print("The initialization criterion has been selected.")
    global init_criterion
    init_criterion = init.get()

def makeform(root, fields):
    entries = []
    for field in fields:
        row = tk.Frame(root)
        lab = tk.Label(row, width=15, text=field, anchor='w', fg='black', font=('arial',16, 'bold'))
        ent = tk.Entry(row)
        row.pack(side=tk.TOP, fill=tk.X, padx=5, pady=5)        
        lab.pack(side=tk.LEFT)                                 
        ent.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)       
        entries.append((field, ent))                            
    return entries

def callback():
    global name 
    name = fd.askopenfilename()
    print(name)

def convert_to_string(scal_crit, cent_crit, init_crit): 
    print("CENT: {}".format(cent_crit))
    print("scal_crit: {}".format(scal_crit))

    if int(scal_crit) == 0:
        scal_crit = 'auto'
    elif int(scal_crit) == 1:
        scal_crit = 'vast'
    elif int(scal_crit) == 2:
        scal_crit = 'pareto'
    elif int(scal_crit) == 3:
        scal_crit = 'range'

    if int(cent_crit) == 0:
        cent_crit = 'mean'
    elif int(cent_crit) == 1:
        cent_crit = 'min'

    if int(init_crit) == 0:
        init_crit = 'kmeans'
    elif int(init_crit) == 1:
        init_crit = 'random'
    elif int(init_crit) == 2:
        init_crit = 'observations'

    return scal_crit, cent_crit, init_crit

def raise_input_error(input_error):
    if input_error == True:
        print("")
        print("")
        print("\t+++ WARNING +++")
        print("One of the input was incorrect, or missing. Please inspect the log to check which input was incorrect or missing.")
        print("Predefined settings were used for the clustering algorithm.")

if __name__ == "__main__":

    root = tk.Tk()

    root.title("Clustering via Local Principal Component Analysis - GUI")

    header = tk.Frame(root, bg='gray', height=30)
    header.pack(fill='both') #, side='top')

    footer = tk.Frame(root, bg='gray', height=30)
    footer.pack(fill='both', side='bottom')

    text1 = tk.Text(root, height=20, width=70)
    photo = tk.PhotoImage(file='burn_logo.gif')
    text1.insert(tk.END, '\n')
    text1.image_create(tk.END, image=photo)
    text1.pack(side=tk.LEFT)

    message = "\n"
    msg = tk.Message(root, text=message)
    msg.config(fg='black', font=('times', 20, 'italic'))
    msg.pack()

    cent = tk.IntVar()
    cent.set(0) #initialize one choice (fill the circle)
    tk.Label(root, text="Centering criterion:", justify=tk.LEFT, padx=20, fg='black', font=('arial',18,'bold')).pack()
    choices_cent = [
        ("Mean"),
        ("Min"),
    ]
    for val2, choice_cent in enumerate(choices_cent):
        tk.Radiobutton(root, text=choice_cent, padx=20, variable=cent, command=GetChoice_cent, value=val2, fg='black', font=('arial',16)).pack(anchor=tk.W)

    message = "\n"
    msg = tk.Message(root, text=message)
    msg.config(fg='black', font=('times', 20, 'italic'))
    msg.pack()

    scal = tk.IntVar()
    scal.set(0) #initialize one choice (fill the circle)
    tk.Label(root, text="Scaling criterion:", justify=tk.LEFT, padx=20, fg='black', font=('arial',18,'bold')).pack()
    choices = [
        ("Auto"),
        ("Vast"),
        ("Pareto"),
        ("Range"),
    ]
    for val, choice_scal in enumerate(choices):
        tk.Radiobutton(root, text=choice_scal, padx=20, variable=scal, command=GetChoice_scal, value=val, fg='black', font=('arial',16)).pack(anchor=tk.W)

    message = "\n"
    msg = tk.Message(root, text=message)
    msg.config(fg='black', font=('times', 20, 'italic'))
    msg.pack()
    
    init = tk.IntVar()
    init.set(0) #initialize one choice (fill the circle)
    tk.Label(root, text="Initialization criterion:", justify=tk.LEFT, padx=20, fg='black', font=('arial',18,'bold')).pack()
    choices = [
        ("K-means"),
        ("Random"),
        ("Observations"),
    ]
    for val, choice_init in enumerate(choices):
        tk.Radiobutton(root, text=choice_init, padx=20, variable=init, command=GetChoice_init, value=val, fg='black', font=('arial',16)).pack(anchor=tk.W)

    message = "\n"
    msg = tk.Message(root, text=message)
    msg.config(fg='black', font=('times', 20, 'italic'))
    msg.pack()

    fields = 'Number of clusters:', 'Number of PCs:'
    ents = makeform(root, fields)

    message = "\n"
    msg = tk.Message(root, text=message)
    msg.config(fg='black', font=('times', 20, 'italic'))
    msg.pack()
    
    search_X = tk.Button(root, text='Select training dataset', command=callback, height=2, width=25, fg='black', font=('arial',16, 'bold'))
    search_X.pack()
    
    message = "\n"
    msg = tk.Message(root, text=message)
    msg.config(fg='black', font=('times', 20, 'italic'))
    msg.pack()

    b3 = tk.Button(root, text='QUIT', command=root.quit, fg='red', font=('arial',18,'bold'))
    b3.pack(side=tk.LEFT, padx=5, pady=5)
    
    b2 = tk.Button(root, text='START', command=root.quit, fg='green', font=('arial',18,'bold'))
    b2.pack(side=tk.RIGHT, padx=5, pady=5)

    root.mainloop()

    input_error = False

    if 'scaling_criterion' not in globals() or 'centering_criterion' not in globals() or 'init_criterion' not in globals():
        scaling_criterion = 'auto'
        centering_criterion = 'mean'
        init_criterion = 'kmeans'
        input_error = True
        print("The definition of the preprocessing criteria was missing, or incomplete. The algorithm will run with the predefined settings:")
        print("\tCentering criterion: mean.")
        print("\tScaling criterion: auto.")
        print("\tInitialization criterion: K-means.")

    try: 
        k = int(ents[0][1].get())
        print("Selected number of clusters: {}".format(k))
    except:
        k = 4
        input_error = True
        print("The value of the number of clusters was missing, or incorrect. The algorithm will run with the predefined setting:")
        print("\tNumber of clusters: 4.")

    
    try: 
        n_PCs = int(ents[1][1].get())
        print("Selected number of PCs: {}".format(n_PCs))
    except:
        n_PCs = 1
        input_error = True
        print("The value of the number of PCs was missing, or incorrect. The algorithm will run with the predefined setting:")
        print("\tNumber of PCs: 1.")

    if 'name' not in globals():
        error_root = tk.Tk()
        error_root.title("FATAL ERROR")
        message = "Exception: Matrix path not set.\t"
        msg = tk.Message(error_root, text=message)
        msg.config(fg='red', font=('times', 30, 'bold'))
        msg.pack()
        b_error = tk.Button(error_root, text='Go back to the main menu.', command=error_root.quit, bg='#0052cc', fg='black', font=('arial',18,'bold'))
        b_error.pack(side=tk.RIGHT, padx=5, pady=5)
        error_root.geometry("500x200")
        error_root.mainloop()
        exit()

    scal_crit, cent_crit, init_crit = convert_to_string(scaling_criterion, centering_criterion, init_criterion)

    try:
        print("Reading training matrix..")
        X = np.genfromtxt(name, delimiter= ',')
    except OSError:
        print("Could not open/read from the selected path: " + name)
        exit()
    
    try:

        model = clustering.lpca(X)
        model.centering = cent_crit
        model.scaling = scal_crit
        model.clusters = k
        model.eigens = n_PCs
        model.initialization = init_crit
        model.correction = "off"                        # -->  to eventually add corrections menu, once tested

        model.fit()
    except:
        raise_input_error(input_error)
        exit()